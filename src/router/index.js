import Vue from 'vue'
import Router from 'vue-router'
import Sites from '@/components/Sites'
import Device from '@/components/Device'
import PageNotFound from '@/components/404'

Vue.use(Router)

/* Vue-router is used for scalability */
export default new Router({
	mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/sites' //For now it redirects to the page for sites but can easily be changed to some kind of homepage
    },
    {
    	path: '/sites',
    	name: 'Sites',
    	component: Sites
    },
    {
    	path: '/device/:id',
    	name: 'Device',
    	component: Device
    },
    {
    	path: '*',
    	name: '404',
    	component: PageNotFound //Default component if no other could be found
    }
  ]
})
